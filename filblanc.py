import json, os

name = input("Ajouter votre aliment : ")
proteine = input("Proteines : ")
glucide = input("Glucides : ")
lipides = input("Lipides : ")
cal = 4*(glucide+proteine)+9*(lipides)

aliment = {'nom': name, 'protéines': proteine, 'glucides': glucide, 'lipides': lipides, 'calories': cal}

aliments = []
aliments.append(aliment)

with open("liste_aliments.json", "w", encoding="utf-8") as save:
    json.dump(aliments, save, indent=4)